#ifndef SUDOKU_H
#define SUDOKU_H

#include <cstdbool>

#define N 9

/* Takes a partially filled-in grid and attempts
to assign values to all unassigned locations in
such a way to meet the requirements for
Sudoku solution (non-duplication across rows,
columns, and boxes) */
bool Solve(int grid[N][N]);

#endif
