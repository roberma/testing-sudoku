# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.20.0)
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(sudoku_test)
include_directories(/Users/robertas/zephyr-sdk/arm-zephyr-eabi/arm-zephyr-eabi/include/c++/10.3.0) 
include_directories(/Users/robertas/zephyr-sdk/arm-zephyr-eabi/arm-zephyr-eabi/include/c++/10.3.0/arm-zephyr-eabi) 

FILE(GLOB app_sources src/*.cpp)
target_sources(app PRIVATE ${app_sources})


