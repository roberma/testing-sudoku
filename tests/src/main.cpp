/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <ztest.h>
#include "../../src/sudoku.h"
#include "../../src/sudoku.cpp"

// initial sudoku number set used in most testcases
int grid[N][N] = {	{0,0,1,0,6,0,0,2,0},
					{0,3,4,0,0,0,0,0,0},
					{0,0,0,0,0,0,0,3,0},
					{1,2,0,0,7,5,0,9,0},
					{4,5,0,8,0,0,0,0,7},
					{7,8,9,0,1,0,2,0,5},
					{0,1,0,0,0,4,0,6,0},
					{6,4,0,0,3,7,0,8,1},
					{9,0,8,6,0,1,4,0,3}};

static void test_usedInRow(void)
{
	// assert that function UsedInRow returns true when the given number is found in specified row 
	int row = 0;
	int num = 1;
	bool cond = UsedInRow(grid, row, num);
	zassert_true(cond, "UsedInRow doesn't return true although the number(%d) is found in specified row(%d)", num, row);
	
	row = 8;
	num = 9;
	cond = UsedInRow(grid, row, num);
	zassert_true(cond, "UsedInRow doesn't return true although the number(%d) is found in specified row(%d)", num, row);
	
	// assert that function UsedInRow returns false when the given number isn't found in specified row 
	row = 0;
	num = 3;
	cond = UsedInRow(grid, row, num);
	zassert_false(cond, "UsedInRow returns true although the number(%d) doesn't exist in specified row(%d)", num, row);

	row = 8;
	num = 7;
	cond = UsedInRow(grid, row, num);
	zassert_false(cond, "UsedInRow returns true although the number(%d) doesn't exist in specified row(%d)", num, row);
}

static void test_usedInCol(void)
{
	// assert that function usedInCol returns true when the given number is found in specified col 
	int col = 0;
	int num = 1;
	bool cond = UsedInCol(grid, col, num);
	zassert_true(cond, "UsedInCol doesn't return true although the number(%d) is found in specified col(%d)", num, col);
	
	col = 8;
	num = 3;
	cond = UsedInCol(grid, col, num);
	zassert_true(cond, "UsedInCol doesn't return true although the number(%d) is found in specified col(%d)", num, col);
	
	// assert that function usedInCol returns false when the given number isn't found in specified col 
	col = 0;
	num = 3;
	cond = UsedInCol(grid, col, num);
	zassert_false(cond, "UsedInCol returns true although the number(%d) doesn't exist in specified col(%d)", num, col);
	
	col = 8;
	num = 9;
	cond = UsedInCol(grid, col, num);
	zassert_false(cond, "UsedInCol returns true although the number(%d) doesn't exist in specified col(%d)", num, col);
}

static void test_usedInBox(void)
{
	// assert that function UsedInBox returns true when the given number is found in specified box
	int row = 0;
	int col = 0;
	int num = 1;
	bool cond = UsedInBox(grid, row, col, num);
	zassert_true(cond, "UsedInBox doesn't return true although the number(%d) is found in specified box(row:%d, col:%d)", num, row, col);
	
	row = 6;
	col = 3;
	num = 7;
	cond = UsedInBox(grid, row, col, num);
	zassert_true(cond, "UsedInBox doesn't return true although the number(%d) is found in specified box(row:%d, col:%d)", num, row, col);	

	// assert that function UsedInBox returns false when the given number isn't found in specified box
	row = 6;
	col = 0;
	num = 2;
	cond = UsedInBox(grid, row, col, num);
	zassert_false(cond, "UsedInBox returns true although the number(%d) doesn't exist in specified box(row:%d, col:%d)", num, row, col);
	
	row = 6;
	col = 6;
	num = 5;
	cond = UsedInBox(grid, row, col, num);
	zassert_false(cond, "UsedInBox returns true although the number(%d) doesn't exist in specified box(row:%d, col:%d)", num, row, col);
}

static void test_findUnassigned(void)
{
	// assert that function FindUnassignedLocation returns true when the unassigned entry is found
	int row;	
	int col; 
	bool cond = FindUnassignedLocation(grid, row, col);
	zassert_true(cond, "FindUnassignedLocation doesn't return true although unassigned entry is found");

	// sudoku grid set with no unassigned entries remaining 
	int grid_assign[N][N] = {	{1,2,3,4,5,6,7,8,9},
								{2,3,4,5,6,7,8,9,1},
								{3,4,5,6,7,8,9,1,2},
								{4,5,6,7,8,9,1,2,3},
								{5,6,7,8,9,1,2,3,4},
								{6,7,8,9,1,2,3,4,5},
								{7,8,9,1,2,3,4,5,6},
								{8,9,1,2,3,4,5,6,7},
								{9,1,2,3,4,5,6,7,8}};

	// assert that function FindUnassignedLocation returns false when no unassigned entries remain
	cond = FindUnassignedLocation(grid_assign, row, col);
	zassert_false(cond, "FindUnassignedLocation returns true although no unassigned entries remain");
}

static void test_isSafe(void)
{
	// assert that function isSafe returns true when it will be legal assign the number to the given location
	int row = 0;
	int col = 0;
	int num = 5;
	bool cond = isSafe(grid, row, col, num);
	zassert_true(cond, "isSafe doesn't return true although it will be legal assign the number(%d) to the given location(row:%d, col:%d)", num, row, col);
	
	row = 8;
	col = 7;
	num = 5;
	cond = isSafe(grid, row, col, num);
	zassert_true(cond, "isSafe doesn't return true although it will be legal assign the number(%d) to the given location(row:%d, col:%d)", num, row, col);
	
	// assert that function isSafe returns false when it won't be legal assign the number to the given location
	row = 0;
	col = 0;
	num = 1;
	cond = isSafe(grid, row, col, num);
	zassert_false(cond, "isSafe returns true although it won't be legal assign the number(%d) to the given location(row:%d, col:%d)", num, row, col);

	row = 6;
	col = 8;
	num = 5;
	cond = isSafe(grid, row, col, num);
	zassert_false(cond, "isSafe returns true although it won't be legal assign the number(%d) to the given location(row:%d, col:%d)", num, row, col);

	row = 8;
	col = 6;
	num = 5;
	cond = isSafe(grid, row, col, num);
	zassert_false(cond, "isSafe returns true although it won't be legal assign the number(%d) to the given location(row:%d, col:%d)", num, row, col);
}

static void test_Solve(void)
{
	// assert that function solve returns true when sudoku is solved
	bool cond = Solve(grid);

	zassert_true(cond, "Solve doesn't return true although sudoku is solved");
	
}

void test_main(void)
{
	ztest_test_suite(framework_tests,
		ztest_unit_test(test_usedInRow),
		ztest_unit_test(test_usedInCol),
		ztest_unit_test(test_usedInBox),
		ztest_unit_test(test_findUnassigned),
		ztest_unit_test(test_isSafe),
		ztest_unit_test(test_Solve)
	);

	ztest_run_test_suite(framework_tests);
}
