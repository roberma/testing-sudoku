#include "profiling.h"

timing_t start_time = 0,    // start time
         end_time = 0;      // stop time
unsigned long long total_time = 0;  // calculated execution time

/* Starts time profiling
 * the timer is initialized and started
 * start time is saved in global variable
 * last result value is reseted
 */
void start(void)
{
    timing_init();
    timing_start();
    start_time = timing_counter_get();
    total_time = 0;
}

/* Stops time profiling
 * end time is saved in global variable
 * the timer is stopped
 */
void stop(void)
{
    end_time = timing_counter_get();
    timing_stop();
}

/* Returns code execution time between start and stop functions in microseconds
 */
unsigned long long result(void)
{
    uint64_t total_cycles;
    uint64_t total_ns;

    // check if stop function was executed last, otherwise return total_time = 0
    if(end_time > start_time)
    {
        total_cycles = timing_cycles_get(&start_time, &end_time);
        total_ns = timing_cycles_to_ns(total_cycles);
        total_time = total_ns / 1000U;
    }
	return total_time;
}