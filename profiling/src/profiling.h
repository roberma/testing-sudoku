#ifndef PROFILING_H
#define PROFILING_H
/* Use extern "C" when timing function libraries are used in C++ code
*/
#if __cplusplus
extern "C" {
#endif

#include <kernel.h>
#include <timing/timing.h>

#if __cplusplus
}
#endif 

void start(void);	// starts time profiling
void stop(void);	// stops time profiling
unsigned long long result(void);	// returns code execution time between start and stop functions

#endif // PROFILING_H