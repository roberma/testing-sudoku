#include <shell/shell.h>
#include <string.h>
#include "../../src/sudoku.h"
#include "../../profiling/src/profiling.h"
#include "shell_interface.h"

/* reads command argument and converts it to dataset row
 * function checks if argument arg has valid: first character is '[', last character is ']', has 9 symbols, has numbers from 1 to 9 or '.'
 * Arguments:
 * arg - character string griven as shell command argument
 * grid - sudoku dataset row where converted argument values are saved 
 * Return:
 * 0 - if dataset is valid
 * len - if dataset length is wrong
 * -1 - if dataset first symbol isn't '[' or last symbol isn't ']'
 * -2 - if dataset has other symbols as than numbers 1-9 or '.'
 */
int read_arg(char *arg, int grid[N])
{
    int len = strlen(arg);
	if(len == ARG_SIZE)
	{
		if(arg[0] != '[' && arg[ARG_SIZE] != ']')
		{
			return -1;
		}
		for(int i = 0; i < N; i++)
		{
			if(arg[i+1] < '1' || arg [i+1] > '9')
			{
				if(arg[i+1] == '.')
				{
					grid[i] = 0;
				}
				else
				{
					return -2;
				}
			}	
			else
			{
				grid[i] = arg[i+1] - '0';
			}
		}
		len = 0;
	}
    return len;
}

/* shell command "sudoku solve" handler
 * reads arguments and converts them into sudoku dataset rows
 * prints error messages when arguments have wrong format or length
 * solves inserted sudoku dataset
 * calculates excution time
 * prints if suduko was able to solve and prints the solution
 * Arguments:
 * arg - character string griven as shell command argument
 * grid - sudoku dataset row where converted argument values are saved 
 * Return:
 * 0 - if dataset is valid
 * len - if dataset length is wrong
 * -1 - if dataset first symbol isn't '[' or last symbol isn't ']'
 * -2 - if dataset has other symbols as than numbers 1-9 or '.'
 */
static int cmd_sudoku_solve(const struct shell *shell, size_t argc, char **argv)
{
	int grid[N][N];	// sudoku dataset
	bool solved;	// if sudoku dataset is valid
	long long unsigned execution_time;	// sudoku execution time
    int ret = 0;	// function read_arg return value  
    
	for (unsigned int i = 1; i < argc; i++)
    {
		ret = read_arg(argv[i], grid[i-1]);
		if (ret < 0)
		{
			// Dataset row has wrong format
			shell_error(shell, "Dataset row %d has wrong format", i);
			return -1;
		}
		else if (ret > 0)
		{
			// Dataset row has wrong length
			shell_error(shell, "Dataset row %d has wrong length", i);
			return -1;
		}
	}
	// print inserted sudoku dataset
    shell_print(shell, "Sudoku dataset inserted:");	
	for (int row = 0; row < N; row++)
	{
		for (int col = 0; col < N; col++)
		{
			shell_fprintf(shell, SHELL_NORMAL, "%d ", grid[row][col]);
		}
		shell_fprintf(shell, SHELL_NORMAL, "\n");
	}
	shell_fprintf(shell, SHELL_NORMAL, "\n");
	// start time profiling
	start();
	// solve inserted sudoku dataset
	solved = Solve(grid);
	// stop sudoku profiling
	stop();
	// calculate execution time
	execution_time = result();
	if (solved)
	{
		// print sudoku execution time and the solution
	    shell_print(shell, "Sudoku solved successfully in %llu microseconds", execution_time);
		shell_print(shell, "Sudoku solution:");		
		for (int row = 0; row < N; row++)
		{
			for (int col = 0; col < N; col++)
			{
				shell_fprintf(shell, SHELL_NORMAL, "%d ", grid[row][col]);
			}
			shell_fprintf(shell, SHELL_NORMAL, "\n");
		}
	}
	else
	{
		// print that dataset is not valid and print the execution time trying to solve sudoku
		shell_print(shell, "Sudoku dataset is not valid: impossible to solve");
		shell_print(shell, "Execution time %llu microseconds", execution_time);
	}

    return 0;
}
// creating subcommand "solve" for sudoku command.
SHELL_STATIC_SUBCMD_SET_CREATE(sub_sudoku,
   SHELL_CMD_ARG(solve, NULL, "Solve sudoku dataset. Dataset input format : [34...8..5] [......4..] [12.....8.] ...", cmd_sudoku_solve, 10, 0),
   SHELL_SUBCMD_SET_END);
// create root command "sudoku"
SHELL_CMD_REGISTER(sudoku, &sub_sudoku, "Sudoku commands", NULL);

void main(void)
{
}
