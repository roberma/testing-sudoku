#ifndef SHELL_INTERFACE_H
#define SHELL_INTERFACE_H

#define ARG_SIZE N+2 // "sudoku solve" command argument size must be sudoku dataset size + 2 ('[' + ']')

int read_arg(char *arg, int grid[N]); // reads command argument and converts it to dataset row
static int cmd_sudoku_solve(const struct shell *shell, size_t argc, char **argv); // solves sudoku dataset inserted as shell command arguments

#endif