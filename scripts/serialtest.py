import serial
import sys

'''Convert string line read from file to dataset of list data type'''
def convert_line_to_list(str_line):
    line = []
    if(str_line[0] == '[' and str_line[1] == '['):
        start = 1
    else:
        print("file has wrong format")
        sys.exit()
    end = len(str_line) - 2
    while str_line[end+1] != ']':
        start = str_line.index('[',start) + 1
        end = str_line.index(']',start)
        line.append(str_line[start:end].split(','))
        start = end + 1
    return line

'''Convert dataset to shell dataset format
   list_line - dataset as list to convert from
   returns dataset converted into character string formated for shell command'''
def convert_line_to_sudoku(list_line):
    sudoku_set = ''
    for row in list_line:
        sudoku_set += ' ['
        for col in row:
            if col == '0': # convert all '0' to  '.'
                sudoku_set += '.'
            elif col == ' ': # strip all spaces from input dataset - wrong formated input file
                sudoku_set += ''
            else:
                 sudoku_set += col
        sudoku_set += ']'
    return sudoku_set         

''' Read input file, send it's content to device and write input and output stream to log file'''
def read_file(file, log_file, port):
    while True:
        line = file.readline()
        if line == '': # EOF found
            break
        dataset = convert_line_to_list(line) # save string line as list 
        sudoku_set = convert_line_to_sudoku(dataset) # convert dataset to shell dataset format
        uart_output = send_sudoku_cmd(sudoku_set, port) # send dataset with sudoku solve command to device and save response from serial port
        log_file.write(uart_output) # write stream from serial port to log file
    file.close()

''' Send suduko solve command to serial port with dataset
    dataset - sudoku dataset for solve command
    port - serial port to send command
    returns input and output stream string'''
def send_sudoku_cmd(dataset, port):
    read_str = '' # string to save input and output stream 
    cmd = "sudoku solve" + dataset + "\r\n" # shell command
    port.write(cmd.encode()) # sent command
    while True:
        byte = port.read() # read data stream sent from device
        read_str += byte.decode()
        ''' if all data received, print it'''
        if byte == b'' and read_str != '':
            print(read_str, end = '', flush=True)
            ''' if prompt found command execution is finished'''
            if 'uart:~$' in read_str[-15:-1]:
                break
            read_str = ''
    return read_str

''' Read user input from console'''
def read_write(log_file, input_file, port):
    ''' if no input file given send \r\n to get shell prompt (user will know that connection is alive'''
    if(input_file == ''):
        cmd = "\r\n"
        port.write(cmd.encode())
    read_str = ''
    user_input = ''
    cmd_len = 1
    while True:
        byte = port.read() # read received stream from board
        read_str += byte.decode(); # convert this stream to string
        '''if all input stream has been read print it'''
        if byte == b'' and cmd_len > 0:
            '''if command inserted interactively was sent, it is already printed, shell echo's it, so don't print it for second time'''
            if read_str[:cmd_len] == user_input:
                print_out = read_str[cmd_len:]
            else:
                print_out = read_str
            print(print_out, end = '', flush=True)
            log_file.write(read_str) # write input and output stream to log file
            '''if shell prompt found then all data is received and user can insert other command'''
            if 'uart:~$' in read_str[-15:-1]:
                user_input = input()
                if user_input == "quit": # exit programm 
                    log_file.close()   # close log file (also saves the file)
                    sys.exit()
                user_input += "\r\n" # add newline sequence to command
                cmd_len = port.write(user_input.encode()) # send new user command
            read_str = ''

''' main routine'''
filename ='' # input file name
interactive = False # flag, if enable interactive input to user
log_filename = 'log.txt' # output
log_file = open(log_filename, "w")
port = serial.Serial("/dev/cu.usbmodem0009601053325", baudrate=115200, timeout=0.2) # connect to board via usb
print("Connected to serial port: " + port.name)
for i in range(len(sys.argv)):
    ''' Enable shell interface input for user'''
    if sys.argv[i] == '-i':
        interactive = True
    ''' Read input from file'''
    if sys.argv[i] == '-f':
        if len(sys.argv) > i + 1:
            filename = sys.argv[i + 1]
            file = open(filename, 'r')
            read_file(file, log_file, port)
        else:
            print("Insert filename after '-f'") 
if interactive: 
    read_write(log_file, filename, port)
else:
    log_file.close()