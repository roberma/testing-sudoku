# Find Zephyr. This also loads Zephyr's build system.

set(BOARD nrf5340dk_nrf5340_cpuapp)

cmake_minimum_required(VERSION 3.13.1)
find_package(Zephyr)
project(Testing)
include_directories(~/zephyr-sdk-0.13.0/arm-zephyr-eabi/arm-zephyr-eabi/include/c++/10.3.0) 
include_directories(~/zephyr-sdk-0.13.0/arm-zephyr-eabi/arm-zephyr-eabi/include/c++/10.3.0/arm-zephyr-eabi)

# Add your source file to the "app" target. This must come after
# find_package(Zephyr) which defines the target.
target_sources(app PRIVATE src/sudoku.cpp src/sudoku.h)