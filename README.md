# Unit tests, Zephyr API, Phyton scripts

## Task 1
Write module in C++ that solves [sudoku] puzzle. Run it on Nordic nRF5340DK development kit. Use [Zephyr OS] project.

### Task Solution
Sudoku module have been written and executed on Nordic board. Module testing was not performed and module may have bugs. Sudoku module takes dataset array and returns true, if the dataset is posible to solve and false otherwise. </br>
Sudoku module can be found [here](src).

## Task 2
Write Unit tests of sudoku module. Run [Zephyr twister] tool to check whether tests pass (use testing on a hardware). Fix bugs, that were found after testing. Reach test [coverage] at least 90% using [Zephyr twister] tool. Generate [coverage] report using [Zephyr twister] tool (HTML format).

### Task Solution
To test sudoku module 6 tests have been written (one for each function):
- UsedInRow testcases:
  - 2 different testcases assert that function returns true when the given number is found in specified row;
  - 2 different testcases assert that function return false when the given number isn't found in specified row;
- UsedInCol testcases:
  - 2 different testcases assert that function returns true when the given number is found in specified column;
  - 2 different testcases assert that function return false when the given number isn't found in specified column;
- UsedInBox testcases:
  - 2 different testcases assert that function returns true when the given number is found in specified box;
  - 2 different testcases assert that function return false when the given number isn't found in specified box;
- FindUnassignedLocation testcases:
  - 1 testcase asserts that function returns true when unassigned entry is found;
  - 1 testcase asserts that function return false when no unassigned entries remain. This testcase uses special sudoku input set where all entries are assigned;
- isSafe testcases:
  - 2 different testcases assert that function returns true when the given number is not already placed in current row, column or box and entry is unassigned;
  - 2 different testcases assert that function return false when the given number is already placed in current row, column or box;
  - 1 testcase asserts that function return false when the given entry location is assigned;
- Solve testcases:
  - 1 testcase asserts that function returns true when sudoku is solved successfully.
  
These testcases try to test each outcome of each branch in the module. Two different testcases are used to test most branch outcomes.</br>
Tests were executed on Nordic nRF5340DK board using Zephyr twister tool.
Found bugs were fixed and coverage report was generated. </br>
Testing results  can be found [here](tests/twister-out). </br>
Coverage report can be found [here](tests/twister-out/coverage_report.png).

## Task 3
Write module that tracks time elapsed between start-stop commands.

Requirements:
  - It must implement _start_, _stop_ and _result_ functions
  - Each start must reset value of result function

### Task Solution
This task was solved using Zephyr API "executing time functions". The time profiling module has three function:
 - Start. Function initializes the timer, starts it, saves start time and resets total time;
 - Stop. Function saves the stop time and stops the timer;
 - Result. Function calculates execution time between start and stop times and returns calculated execution time.</br>
Time profiling module can be found [here](profiling).

## Task 4
Write a shell interface for sudoku module using [Zephyr Shell] API</br>
```bash
# cli call requirement
sudoku solve <dataset>

# potential example
sudoku solve [34...8..5], [......4..], [12.....8.] # rest of data
```
Requirements:
  - Should return status if data set is solvable or not
  - Should return status if provided dataset has incorrect format or incorrect number of digits
  - Should return time (in $\mu s$) attempted to solve sudoku puzzle using time tracking module

After finishing fourth task a working sudoku shell must be written, which solves
a puzzle with prodived dataset. Reports solving status and time elapsed trying
to solve a puzzle.

### Task Solution
Shell interface for sudoku module has been written using Zephyr Shell API. Sudoku solution function is called using global shell command "sudoku solve <dataset>".</br> 
Each command argument is converted into one dataset row. User must provide 9 "sudoku solve" shell command arguments (9 sudoku puzzle rows). Argument can have 9 numbers from 1 to 9 or '.', first symbol must be '[' and last symbol must be ']'. Argument format example is [34...8..5].</br>
Shell interface prints:
 - Sudoku solution or message, that sudoku dataset is not solvable;
 - Error message, if provided dataset has incorrect format or wrong length;
 - Execution time (in $\mu s$) attempted to solve sudoku puzzle.

Shell module can be found [here](shell)

## Task 5
Using [Python], write a script which connects to device over serial port. Implement communication tunnel to pass and retrieve data from sudoku shell over serial port. Executes sudoku shell command on a device Log all outgoing and incoming data to a text file.

### Task Solution

 The script has been written, which connects to device over serial port. Communication tunnel to pass and retrieve data from sudoku shell over serial port has been implemented. Sudoku shell command is sent over serial port to device and Sudoku solution is returned back to the user. All data traffic is written to [log file](scripts/log.txt). Sudoku datasets can be read from file (script must be started with command line argument "-f filename"). User can access shell interface interactively from terminal and there input command and dataset interactively (script must be started with command line argumet "-i"). Both options can be used at the same time. When interactive mode is used, user must insert "quit" command to terminal to end script execution, otherwise log file won't be saved.</br>
Python script can be found [here](scripts)

## Resources:
- Valid dataset for hardware-side testing is stored in [dataset_hw_testing.txt] file.
- Datasets for desktop-side testing are stored in [dataset.txt] file.

</br>
---
[Sudoku]: https://en.wikipedia.org/wiki/Sudoku
[Zephyr OS]: https://docs.zephyrproject.org/latest/
[Zephyr Twister]: https://docs.zephyrproject.org/latest/guides/test/twister.html
[Coverage]: https://docs.zephyrproject.org/latest/guides/test/coverage.html
[Zephyr Shell]: https://docs.zephyrproject.org/latest/reference/shell/index.html
[Python]: https://www.python.org/
[dataset_hw_testing.txt]: resources/dataset_hw_testing.txt
[dataset.txt]: resources/dataset.txt
